# Load the shell dotfiles, and then some:
# * ~/.extra can be used for other settings you don’t want to commit.
for file in ~/.{profile,exports,aliases,functions,extra}; do
  [ -r "$file" ] && [ -f "$file" ] && source "$file"
done
unset file

for file in ~/Google\ Drive/dotfiles/.{aliases,servers,exports}; do
  [ -r "$file" ] && [ -f "$file" ] && source "$file"
done
unset file
