;;
;; ERC Config
;;

;; Determins if erc should auto join on start up
(erc-autojoin-mode t)

;; Modules
(add-to-list 'erc-modules 'notifications)
(add-to-list 'erc-modules 'spelling)
(add-to-list 'erc-modules 'button)

;; Set default nick
(setq erc-nick "mocon")

;; Set default channels to join
(setq erc-autojoin-channels-alist
  '((".*\\.freenode.net" "#clojure" "#clojurescript" "#immutant")))

;; Interpret mIRC-style color commands in IRC chats
(setq erc-interpret-mirc-color t)

;; The following are commented out by default, but users of other
;; non-Emacs IRC clients might find them useful.
;; Kill buffers for channels after /part
(setq erc-kill-buffer-on-part t)
;; Kill buffers for private queries after quitting the server
(setq erc-kill-queries-on-quit t)
;; Kill buffers for server messages after quitting the server
(setq erc-kill-server-buffer-on-quit t)
